import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class VehiculosService {

  //private vehiculosSubject = new BehaviorSubject([]);
  private vehiculos:Vehiculo[] = [];

  constructor(private http: HttpClient) {
    
    console.log("Servicio de vehiculos listo para usar!!!");
  }

  url = "https://rocky-refuge-74084.herokuapp.com/";


  getVehiculos(){
    return this.http.get(`${this.url}vehicles`);
    // return this.http.get('https://rocky-refuge-74084.herokuapp.com/vehicles')
    //return this.vehiculos;
  }

  getVehiculo( idx: string ){
    return this.http.get(`${this.url}vehicles/${idx}`);
    //return this.http.get(`https://rocky-refuge-74084.herokuapp.com/vehicles/${ idx }`);
  }

  buscarVehiculos( termino:string ):Vehiculo[]{

    let vehiculosArr:Vehiculo[] = [];
    this.getVehiculos()
      .subscribe( (data: any) => { 
        this.vehiculos = data;
        termino = termino.toLowerCase();
        for( let i = 0; i < this.vehiculos.length; i ++ ){
          let vehiculo = this.vehiculos[i];
          let brand = vehiculo.brand.toLowerCase();
          if( brand.indexOf( termino ) >= 0  ){
            vehiculo.idx = i;
            vehiculosArr.push( vehiculo )
          }
        }
      },
      (error) => {
        console.error(error);
      }
    );
    return vehiculosArr;
  }

  crearVehiculo( vehiculo_json: Vehiculo_Json){
    console.log("-------------------------------------------------------------",vehiculo_json)
    this.http.post(`${this.url}vehicles`, vehiculo_json);
    // this.http.post('https://rocky-refuge-74084.herokuapp.com/vehicles', vehiculo_json);
  }
  dataVehiculo;

  addVehiculo(vehiculo_json: Vehiculo_Json) {
    console.log("Esta Creando")
    let params = JSON.stringify(vehiculo_json)
    let headers = new HttpHeaders().set('Content-Type','application/json');

    return this.http.post(`${this.url}vehicles`, params, {headers})
    // return this.http.post('https://rocky-refuge-74084.herokuapp.com/vehicles', params, {headers:headers})
    .pipe(
      map( respuesta => {
          return respuesta
        })
      )
  };

  updateVehiculo(vehiculo_json: Vehiculo_Json) {
    console.log("Esta Actualizando")
     let headers = new HttpHeaders().set('Content-Type','application/json');
     let id = vehiculo_json.id;
       delete vehiculo_json.id;
       let params = JSON.stringify(vehiculo_json);
       return this.http.put(`${this.url}vehicles/${id}`, params, {headers})
      // return this.http.put(`https://rocky-refuge-74084.herokuapp.com/vehicles/${id}`, params, {headers:headers})
      .pipe(
        map( respuesta => {
            return respuesta
          })
      )
   };

  deleteVehiculo( idx: string ){
    return this.http.delete(`${this.url}vehicles/${idx}`);
    // return this.http.delete(`https://rocky-refuge-74084.herokuapp.com/vehicles/${ idx }`);
  }
}

export interface Vehiculo{
  id?: number;
  model: string,
  brand: string,
  passengers_capacity: number,
  price: number,
  category: number,
  year: number,
  weight: number,
  idx?: number;
};

export interface Vehiculo_Json{
  id?: number,
  vehicle:{
    brand: string,
    model: string,
    passengers_capacity: number,
    category: number,
    price: number,
    year: number,
    weight: number,
  }
}