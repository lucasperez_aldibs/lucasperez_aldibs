import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehiculosService } from '../../servicios/vehiculos.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  vehiculos:any[] = []
  termino:string;

  constructor( private activatedRoute:ActivatedRoute,
              private _vehiculosService: VehiculosService) {

  }

  ngOnInit() {

    this.activatedRoute.params.subscribe( params =>{
      this.termino =params['termino'];
      this.vehiculos = this._vehiculosService.buscarVehiculos( params['termino'] );
      console.log( this.vehiculos );
    });

  }

}
