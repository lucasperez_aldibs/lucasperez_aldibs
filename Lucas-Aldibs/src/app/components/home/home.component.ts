import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  vehiculos: any[] = [];

  constructor( private http: HttpClient ) { 

    this.http.get('https://rocky-refuge-74084.herokuapp.com/vehicles')
      .subscribe( (resp: any) => {
        this.vehiculos = resp;
      });
  }

  ngOnInit() {
  }

}
