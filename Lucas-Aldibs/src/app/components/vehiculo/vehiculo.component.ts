import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehiculosService } from '../../servicios/vehiculos.service';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html'
})
export class VehiculoComponent {

  vehiculo:any = {};


  constructor( private activatedRoute: ActivatedRoute,
               private _vehiculosService: VehiculosService,
               private router: Router
    ){

    this.activatedRoute.params.subscribe( params =>{
      this._vehiculosService.getVehiculo(  params['id'] )
        .subscribe( (data: any) => {
          this.vehiculo = data;
        },
        (error) => {
          console.error(error);
        })
    });
  }

  EliminarVehiculo(){
    this.activatedRoute.params.subscribe( params =>{
      this._vehiculosService.deleteVehiculo(  params['id'] )
        .subscribe( () => {
          this.router.navigate(['/vehiculos'])
        },
        (error) => {
          console.error(error);
        })
    });
  }

  EditarVehiculo() {
    this.router.navigate( ['/vehiculo-nuevo', this.vehiculo.id] );
  }

}