import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehiculo-tarjeta',
  templateUrl: './vehiculo-tarjeta.component.html',
})
export class VehiculoTarjetaComponent implements OnInit {

  @Input() vehiculo: any = {};
  @Input() index: number;

  @Output() vehiculoSeleccionado: EventEmitter<number>;

  constructor(private router: Router) {
    this.vehiculoSeleccionado = new EventEmitter();
  }

  ngOnInit() {
  }

  verVehiculo() {

    this.router.navigate( ['/vehiculo', this.vehiculo.id] );
  }

}