import { Component, OnInit } from '@angular/core';
import { VehiculosService, Vehiculo, Vehiculo_Json } from '../../servicios/vehiculos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-vehiculo-nuevo',
  templateUrl: './vehiculo-nuevo.component.html',
  styleUrls: ['./vehiculo-nuevo.component.css']
})
export class VehiculoNuevoComponent implements OnInit {

  vehiculo: Vehiculo;
  vehiculo_json: Vehiculo_Json;
  veh;

  constructor( 
    private _vehiculosService:VehiculosService,
    public activatedRoute: ActivatedRoute,
    private router:Router) { 
      this.vehiculo_json = { 
        id: null,
        vehicle:{
          model: "",
          brand: "",
          passengers_capacity: null,
          price: null,
          category: null,
          year: null,
          weight: null,
        }
      };
      this.vehiculo = {
        brand: "",
        model: "",
        passengers_capacity: null,
        category: null,
        price: null,
        year: null,
        weight: null,
      };
      this.veh = [{value: 0, view: "motocicleta"},
        {value: 1, view: "automovil"},
        {value: 2, view: "srv"},
        {value: 3, view: "camioneta"},
        {value: 4, view: "camion"},
        {value: 5, view: "micro"}
      ]
    }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe( params =>{
      const id = params["id"];
    //console.log("Este es El id kkkkkkkkkkkkkkkkkkkkkkk ",id)
    if(id){
      this._vehiculosService.getVehiculo(  params['id'] )
        .subscribe( (data: any) => {
          this.vehiculo = data;
          var i;
          for ( i=0; i < this.veh.length; i++)   {
            if(this.veh[i].view == this.vehiculo.category){this.vehiculo.category = parseInt(this.veh[i].value) }
          };
          var passenger = parseInt( (this.vehiculo.passengers_capacity).toString() );
          var pri = parseInt((this.vehiculo.price.toString())); 
          this.vehiculo_json = {
            vehicle:{
              model: this.vehiculo.model,
              brand: this.vehiculo.brand,
              passengers_capacity: passenger,
              price: pri,
              category: this.vehiculo.category,
              year: this.vehiculo.year,
              weight: this.vehiculo.weight,
            }
          }
        },
        (error) => {
          console.error(error);
        })
      }
    });
    
  }
   submitForm() {
     this.activatedRoute.params.subscribe(  (data) => {
      if (!this.vehiculo.id){
          this._vehiculosService.addVehiculo(  this.vehiculo_json ) 
           .subscribe(  (data: Vehiculo) => {
              this.router.navigate(['/vehiculo',data.id])
           });
        
      }else {
        this.vehiculo_json.id = this.vehiculo.id;
        this._vehiculosService.updateVehiculo(  this.vehiculo_json ) 
          .subscribe(  (data: Vehiculo) => {
            this.router.navigate(['/vehiculo',data.id])
         });
      }
    });
  };

  EliminarVehiculo(){
    this.activatedRoute.params.subscribe( params =>{
      this._vehiculosService.deleteVehiculo(  params['id'] )
        .subscribe( () => {
          this.router.navigate(['/vehiculo'])
        },
        (error) => {
          console.error(error);
        })
    });
  }

  guardar( forma: NgForm) {
    console.log("ACTIVANDO GUARDAR DE FORM ", forma)
          
      }

  }
