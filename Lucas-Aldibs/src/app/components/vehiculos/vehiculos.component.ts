import { Component, OnInit } from '@angular/core';
import { VehiculosService, Vehiculo } from '../../servicios/vehiculos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html'
})
export class VehiculosComponent implements OnInit {

  vehiculos:Vehiculo[] = [];
  loading:boolean = true;

  constructor( private _vehiculosService:VehiculosService,
               private router:Router ) { }

  async  ngOnInit() {

    this._vehiculosService.getVehiculos()
      .subscribe( (data: any) => { 
          setTimeout(() => {
            this.vehiculos = data;
            this.loading = false;
            },400
          )
        },
        (error) => {
          console.error(error);
        }
      );
  }

  verVehiculo( idx:number ){
    this.router.navigate( ['/vehiculo',idx] );
  }

}

