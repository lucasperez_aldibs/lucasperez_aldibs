import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { VehiculosComponent } from './components/vehiculos/vehiculos.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { VehiculoNuevoComponent } from './components/vehiculo-nuevo/vehiculo-nuevo.component';
import { DataComponent } from './components/data/data.component';


const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'vehiculos', component: VehiculosComponent },
    { path: 'vehiculo/:id', component: VehiculoComponent },
    { path: 'vehiculo-nuevo', component: VehiculoNuevoComponent },
    { path: 'vehiculo-nuevo/:id', component: VehiculoNuevoComponent },
    { path: 'buscar/:termino', component: BuscadorComponent },
    { path: 'data', component: DataComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);